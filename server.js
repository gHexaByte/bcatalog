const express = require('express');
const app = express();


var mongoose = require('mongoose');

//Connect to database
mongoose.connect("mongodb://127.0.0.1/bcatalog");
mongoose.connection.on('error', function (err){
    console.log("DB Connection Error: " + err);
    process.exit(2);
});


//Database book schema
var bookSchema = new mongoose.Schema( {
    name: { type: String, default: "no name" },
    author: { type: String, default: "no author" },
    description: { type: String, default: "no description" }
} );
var Book=mongoose.model('Book', bookSchema);

//Allow access to public folder
app.use(express.static('public'));

//Add book to DB. Takes name, author and description from GET query.
app.get('/add_book', function (req, res){
	var newBook = new Book({name: req.query.name,
							author: req.query.author,
							description: req.query.description});
	newBook.save(function (err) {
	  if (err) {
	  	  res.send(err);
	  } else {
	  	  res.send("Book saved!");
	  }
	});
});

//Edit book. Takes id, name, author and description from GET query.
app.get('/edit_book', function (req, res){
	Book.findOne({ _id: req.query.id} , function(err, bk) {
	    if (err) {
	        res.send(err);
	    } else {
	        bk.name=req.query.name;
	        bk.author=req.query.author;
	        bk.description=req.query.description;
	       	bk.save(function (err) {
			if (err) {
			  	  res.send(err);
			  } else {
			  	  res.send("Book edited!");
			  }
			});
	    }
	});
});

//Delete book. Takes id from GET query.
app.get('/del_book', function (req, res){
	Book.remove({ _id: req.query.id} , function(err) {
	    if (err) {
	        res.send(err);
	    } else {
	        res.send("Book removed!");
	    }
	});
});

//Search books from DB. Takes q(search query) from GET query.
app.get('/search', function (req, res){
	var text=req.query.q;
	Book.find(
		{ $or:[ {name: new RegExp( text, "i")}, {author: new RegExp( text, "i")} ]},
		function (err, books) {
	    	return res.json(books);
		}
	);
});

//Redirect to main page
app.get('/', function (req, res) {
	res.redirect('/index.html');
})

//Start app
app.listen(8080, function () {
   console.log('App listening on port 8080!')
});